import re
import csv

input_csv_path = "C:\\Users\glago\Desktop\Hokori Tori 2017 - ВСЯ ИНФА.csv"
output_csv_path = "C:\\Users\glago\Desktop\Hokori Tori 2017 - НОМЕРА.csv"
id_column = "ID"

merged_table = []

# Reading data
with open(input_csv_path, 'r', encoding='utf-8') as f:
    data = csv.reader(f, delimiter=',', quotechar='"')
    rows = [[cell for cell in row] for row in data]
id_column = rows[0].index(id_column)
ids = [row[id_column] for row in rows[1:]]

for id in set(ids):
    rows_to_merge = list(filter(lambda r: r[id_column] == id, rows))
    if len(rows_to_merge) == 1:
        merged_table.append(rows_to_merge[0])
        continue


    def clean(cell):
        ret = cell.strip().replace(' , ', ', ').replace('  ', ' ')
        ret = re.sub(r'(\w),(\w)', r'\1, \2', ret)
        ret = re.sub(r'(\w)/(\w)', r'\1 / \2', ret)
        # print(f"'{cell}'->'{ret}'")
        return ret

    merged_row = [", ".join(sorted(set([clean(cell) for cell in col]))) for col in list(zip(*rows_to_merge))]
    merged_table.append(merged_row)

with open(output_csv_path, 'w', newline='', encoding='utf-8') as f:
    writer = csv.writer(f, delimiter=',', quotechar='"')
    writer.writerow(rows[0])
    writer.writerows(sorted(merged_table, key=lambda a: int(a[0])))
