import os
import shutil
import csv

sources_dir = "H:\\Mailru_Cloud\\Hokori Tori\\2017"
img_exts = {'jpeg', 'png', 'jpg'}
track_exts = {'avi', 'mp4', 'mp3', 'mov', 'wmv'}

# ---------------------------------------------------------------------

list_all_exts = False

collect_images = True
img_list_target_csv = "C:\\Users\\glago\\Desktop\\images.csv"
filler_path = "H:\\HokoriToriLocal\\transperent_px.png"

check_max_img = False

# ---------------------------------------------------------------------

input_csv_path = "C:\\Users\\glago\\Desktop\\Hokori Tori 2017 - НОМЕРА.csv"
id_column = "ID"

check_tracks = False

tracks_output_dir = "H:\\HokoriToriLocal\\Fest\\tracks"
export_tracks = False

# Reading CSV
with open(input_csv_path, 'r', encoding='utf-8') as f:
    data = csv.reader(f, delimiter=',', quotechar='"')
    rows = [[cell for cell in row] for row in data]
id_column = rows[0].index(id_column)
table_data = {int(row[id_column]): {rows[0][j]: row[j] for j in range(1, len(rows[0]))} for row in rows[1:]}
files_data = {}

ids_from_table = set(table_data.keys())
if sorted(ids_from_table) != sorted([int(row[id_column]) for row in rows[1:]]):
    print("DUPLICATE ID!!!!!!!!")
    print(sorted(ids_from_table), '\n', sorted([row[id_column] for row in rows[1:]]))
    exit(1)

ids_from_files = set()

# ---------------------------------------------------------------------

known_exts = set()
max_images = 0 if check_max_img else 2
data = {}
img_list_head = ['id', 'files_nom', 'files_name', 'nom', 'nicks', 'name', 'city']
for i in range(1, max_images + 1):
    img_list_head += [f'img{i}_visible', f'img{i}_path']
img_list = []


def to_filename(string):
    filename = string.encode('cp1251', 'replace').decode('cp1251')
    filename = filename.replace(': ', "-")
    filename = filename.replace(':', "-")
    filename = filename.replace('"', "'")
    filename = filename.replace('/', "-")
    filename = filename.replace('\\', "-")
    filename = ''.join(i if i not in "\*?<>|" else "#" for i in filename)
    return filename


def make_filename(id, nicks, nom, cosband, name_fandom, ext):
    ret = f"ID{id}. {nom} - "
    nicks = nicks.split(', ')
    nicks = ', '.join(nicks[:2]) + ',' if len(nicks) > 3 else d['ник']
    ret += nicks
    if cosband:
        ret += f" [{to_filename(cosband).replace(' - ', '-')}]"
    name = name_fandom.replace(' - ', '-')
    if ' / ' in name:
        name = name.split(' / ', 1)[0]
    ret += f" - {name}"
    ret = to_filename(ret).strip()

    if ret.count(' - ') > 2:
        print("CHECK FIELDING!!!! ", ret)

    return f"{ret}.{ext}"


def split_name(dir_name):
    try:
        id, name = dir_name.split(' ', 1)
        id = int(id)
        return id, name
    except (ValueError, KeyError) as e:
        print("[Failed to parse]", dir_name, "\n", e)
        return None, None


for nom_dir in os.listdir(sources_dir):
    item_dirs = os.listdir(os.path.join(sources_dir, nom_dir))
    for item_dir in item_dirs:
        files = os.listdir(os.path.join(sources_dir, nom_dir, item_dir))
        if list_all_exts:
            known_exts |= {file.rsplit('.', 1)[-1].lower() for file in files}
            continue

        id, ref_name = split_name(item_dir)
        ids_from_files.add(id)
        files_data[id] = os.path.join(nom_dir, item_dir)


        def filter_files(extensions, l):
            return list(filter(lambda f: f.rsplit('.', 1)[-1].lower() in extensions, l))


        # --- images ---

        if collect_images:

            images = filter_files(img_exts, files)
            if check_max_img and len(images) > max_images:
                max_images = len(images)
                continue

            img_paths = [os.path.join(sources_dir, nom_dir, item_dir, image) for image in images]
            if any([not os.path.isfile(path) for path in img_paths]):
                print("INTERNAL ERROR!!!!!!!!")
                exit(1)

            nicks = table_data[id]['ник']
            if table_data[id]['косбенд']:
                nicks += f' [{table_data[id]["косбенд"]}]'

            row_left = [id, nom_dir, ref_name, table_data[id]["Номинация"], nicks,
                        table_data[id]["название / фендом"], table_data[id]["город"]]
            row_right = []
            for i in range(max_images):
                img_path = filler_path
                img_visible = False

                try:
                    img_path = img_paths[i]
                    img_visible = True
                except IndexError:
                    pass

                row_right += ["true" if img_visible else "false", img_path]

            img_list.append(row_left + row_right)

        # --- tracks ---

        if check_tracks or export_tracks:
            tracks = filter_files(track_exts, files)
            track_paths = [os.path.join(sources_dir, nom_dir, item_dir, file) for file in tracks]
            if any([not os.path.isfile(path) for path in track_paths]):
                print("INTERNAL ERROR!!!!!!!!")
                exit(1)

        if check_tracks:
            exts = ', '.join([f.rsplit('.', 1)[-1].lower() for f in tracks])
            if len(tracks) != 1:
                print(f"{exts if exts else 'EMPTY'}: {os.path.join(nom_dir, item_dir)} - {table_data[id]}")

        if export_tracks:
            for track_path in track_paths:
                ext = track_path.rsplit('.', 1)[-1]
                d = table_data[id]
                new_name = make_filename(id, d['ник'], d['Номинация'], d['косбенд'], d['название / фендом'], ext)

                print(track_path, '->')
                track_new_path = os.path.join(tracks_output_dir, new_name)
                print(track_new_path, '\n')
                # shutil.copy(track_path, track_new_path)

if check_tracks and ids_from_table != ids_from_files:
    print('\nIn table but not in files:', {id: table_data[id] for id in ids_from_table - ids_from_files})
    print('In files but not in table:', {id: files_data[id] for id in ids_from_files - ids_from_table})
if list_all_exts:
    print(known_exts)
if check_max_img:
    print('max_images:', max_images)
if collect_images:
    with open(img_list_target_csv, 'w', newline='', encoding='utf-8') as f:
        writer = csv.writer(f, delimiter=',', quotechar='"')
        writer.writerow(img_list_head)
        writer.writerows(sorted(img_list, key=lambda a: int(a[0])))

