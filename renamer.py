import csv
import os
import re
import shutil

input_csv_path = "C:\\Users\\glago\\Desktop\\Hokori Tori 2017 - НОМЕРА.csv"
id_column, num_column = "ID", "num"

tracks_dir = "H:\\HokoriToriLocal\\Fest\\tracks"
track_filename_re = re.compile("^ID(?P<id>\d{1,2})\. (?P<nom>.*?) - (?P<nicks>.*?) - (?P<name>.*?)(?P<ext>\..{3})$")

zad_dir = "H:\\HokoriToriLocal\\Fest\\zad"
zad_filename_re = re.compile("^num(?P<num>\d{3})(?P<ext>\..{3})$")

def to_filename(string):
    filename = string.encode('cp1251', 'replace').decode('cp1251')
    filename = filename.replace(': ', "-")
    filename = filename.replace(':', "-")
    filename = filename.replace('"', "'")
    filename = filename.replace('/', "-")
    filename = filename.replace('\\', "-")
    filename = ''.join(i if i not in "\*?<>|" else "#" for i in filename)
    return filename


def make_filename(num, nicks, nom, cosband, name_fandom, id, ext):
    ret = f"{num} {nom}. "
    nicks = nicks.split(', ')
    nicks = ', '.join(nicks[:2]) + ',' if len(nicks) > 3 else d['ник']
    ret += nicks
    if cosband:
        ret += f" [{to_filename(cosband).replace(' - ', '-')}]"
    name = name_fandom.replace(' - ', '-')
    if ' / ' in name:
        name = name.split(' / ', 1)[0]
    ret += f" - {name} (ID{id})"
    ret = to_filename(ret).strip()

    if ret.count(' - ') > 2:
        print("CHECK FIELDING!!!! ", ret)

    return f"{ret}{ext}"



# Reading CSV
with open(input_csv_path, 'r', encoding='utf-8') as f:
    data = csv.reader(f, delimiter=',', quotechar='"')
    rows = [[cell for cell in row] for row in data]

id_column = rows[0].index(id_column)
num_column = rows[0].index(num_column)
get_num = {int(row[id_column]): row[num_column] for row in rows[1:]}

table_data = {row[num_column]: {rows[0][j]: row[j] for j in range(len(rows[0]))} for row in rows[1:]}

for track_filename in os.listdir(zad_dir):
    path = os.path.join(zad_dir, track_filename)
    print(path, '->')
    m = re.search(zad_filename_re, track_filename)
    if not m:
        print('NOT MATCH:', track_filename)
        continue
    # new_filename = f"{get_num[int(m.group('id'))]} {m.group('nom')}. {m.group('nicks')} - {m.group('name')} (ID{m.group('id')}){m.group('ext')}"
    d = table_data[m.group('num')]
    new_filename = make_filename(m.group('num'), d['ник'], d['Номинация'], d['косбенд'], d['название / фендом'], d['ID'], m.group('ext'))
    new_path = os.path.join(zad_dir, new_filename)
    print(new_path, '\n')
    shutil.move(path, new_path)
